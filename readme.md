# Ejemplo SOLID

_Proyecto Backend como ejemplo de definicion de los Principios SOLID, en este ejemplo nos enfocaremos en una funcionalidad básica que son el calculo de áreas y perímetros de figuras geométricas_

_SOLID es una terminología que detalla 5 principios basicos de la POO y nos ayuda a tener un código mucho mas limpio y mantenible_

_Principios_

```
S -> Single Responsability Principle (SRP) / Principio de Responsabilidad Unica
O -> Open Closed Principle (OCP) / Principio de abierto/cerrado
L -> Liskov Substitution Principle (LSP) / Principio de Sustitucion de Liskov
I -> Interface Segregation Principle (ISP) / Principio de Segregación de Interfaces
D -> Dependency Inversion Principle (DIP) / Principio de inversion de dependencias
```


## Comenzando 🚀

_Una pequeña descripción por cada acrónimo del Principio SOLID_

```
S: Nos indica que una clase solo debe tener una única razón para existir, por ejemplo solo almacenar datos, hacer consultas web, o un grupo específico de operaciones,
   siguiendo este principio no deberias acumular todo tu codigo en una sola parte, sino desacoplarla.
   Un claro ejemplo lo podemos ver en la clase Rectangle que se desacopla las operaciones de perimetro y area en otras clases
	
O: Nos indica que el codigo que elaboramos debe estar abierto a la extensión pero cerrado a la modificación,es decir se puede expandir la funcionalidad de unmodulo
   sin tener que editar el codigo original.
   El ejemplo lo podemos ver en las clases AreaOper,PerimOper y IGeometricShape, a futuro cuando se agreguen nuevas funionalidades, solo tendran que implementar la
   interfaz y no será necesario cambiar la funcionaldiad original.

L: Nos indica que una clase heredada no debe modificar la funcionalidad de la clase que hereda, solo debe permitir agregar nuevas funcionaldades
   Por ejemplo se crea una Clase Square que herede de Rectangle, es valido pero se modificaría las propiedades de la clase Padre Rectangle.
   
I: Nos indica que solo debemos brindar solo la información solicitada, no brindar mas informacion de la necesaria, para esto debemos separar lo mas posible las interfaces
   o abstracciones,un ejemplo lo podemos ver en la interfaz IGeometricShape que se desglosa por funcionalidad, ya que si solo se usa para calcular el area, segun este
   principio no deberia tener mas operaciones asociadas al area, por ejemploel perimetro deberia ir en otra interface.
   
D: Nos indica que debemos reducir la dependencia que exite entre nuestros modulos, debe usar los objetos que utiliza y no estar encargado de crearlos.
   un ejemplo lo podemos ver en la clase GreatCalculator que calcula las operaciones , independientemente del objetoque se le envie mediante una interface

```

## Construido con 🛠️

_Herramientas utilizadas en el desarrollo del proyecto_

```
* Java 8
* Spring Framework
* Spring Boot
* Spring MVC
* Lombok
* Postman para las pruebas
```


## Autor ✒️

* **Marino Alonso Z.Y. **

_Links:_
* [Linkedin](https://www.linkedin.com/in/marino-alonso-zevallos-yapias-82a200196/)
* Bitbucket : [azevallosyapias](https://bitbucket.org/azevallosyapias/)
* GitHub: [alonsozy](https://github.com/alonsozy)
* DockerHub: [alonsozy](https://hub.docker.com/u/alonsozy)



