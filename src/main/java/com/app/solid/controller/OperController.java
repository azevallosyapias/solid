package com.app.solid.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.app.solid.entiti.EquilTriangle;
import com.app.solid.entiti.Rectangle;
import com.app.solid.service.GreatCalculator;

@RestController
@RequestMapping("")
public class OperController {
 
	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@GetMapping(path = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public String getOper() {
		log.info("GetMapping - value: /getOper");
		String rpta="";
		
		GreatCalculator great=new GreatCalculator();
		Rectangle r= new Rectangle();
		r.setHeight(5d);
		r.setWidth(3d);
		
		EquilTriangle tri = new EquilTriangle();
		tri.setBase(10d);
		tri.setHeight(50d);
		
		great.calculate(r);
		double areaRec=great.getTotalAreas();
		double perimeterRec = great.getTotalPerimeters();
		
		great.calculate(tri);
		double areaTri=great.getTotalAreas();
		double perimeterTri = great.getTotalPerimeters();
		
		rpta = "Datos: \n"+r.toString()+" - Area: "+areaRec+" - Perimeter: "+perimeterRec+
				"\n"+tri.toString()+" - Area: "+areaTri+" - Perimeter: "+perimeterTri;
		
		return rpta;
	}
	
}
