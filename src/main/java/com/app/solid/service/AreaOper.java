package com.app.solid.service;

import com.app.solid.entiti.Rectangle;
import com.app.solid.entiti.Shape;
import com.app.solid.entiti.EquilTriangle;

public class AreaOper {

	// en el siguiente codigo se puede ver que la logica esta agrupada en una clase,
	// pero se estaria violando el segundo principio
	// de SOLID -> OCP , ya que si esta abierto para la extension se tendria que
	// modificar la funcionalidad original
	// para poder solucionar este problema, se usaria herencia, polimorfismo,
	// abstraccion; en este caso particular se crearia una interfaz
	// con los metodos de calcular el area y perimetro y se implementara

	// public static double calcular(Shape shape) {
//
//		double area = 0d;
//		if (shape instanceof Rectangle) {
//			Rectangle rec = (Rectangle) shape;
//			area = rec.getHeight() * rec.getWidth();
//		} else if (shape instanceof EquilTriangle) {
//			EquilTriangle tri = (EquilTriangle) shape;
//			area = tri.getBase() * tri.getHeight() * 2;
//		} else {
//			area = 0d;
//		}
//		return area;
//	}

	// funcion corregida con la interfaz
	// con esta funcinoalidad se cumple el segundo principio de SOLID (OCP),es decir
	// no importa que clase ingrese al metodo , con tal que implemente la interfaz
	// se
	// cumplira de solo modificar las nuevas clases y no se modificaria este metodo
	// original
	public static double calcular(IFuncArea shape) {
		double area = 0d;
		area = shape.area();
		return area;
	}

}
