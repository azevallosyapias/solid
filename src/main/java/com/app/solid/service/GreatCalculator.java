package com.app.solid.service;

import lombok.Getter;
import lombok.Setter;

//clase para obtener la sumatoria de areas y perimetros
public class GreatCalculator {

	@Getter
	@Setter
	private double totalAreas;

	@Getter
	@Setter
	private double totalPerimeters;

	public void calculate(IGeometricShape shape) {
		AreaOper area = new AreaOper();
		PerimOper perimeter = new PerimOper();
		totalAreas = area.calcular(shape);
		totalPerimeters = perimeter.calcular(shape);
	}

}
