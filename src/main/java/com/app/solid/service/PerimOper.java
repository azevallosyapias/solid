package com.app.solid.service;

import com.app.solid.entiti.Rectangle;

public class PerimOper {

	// en el siguiente codigo se puede ver que la logica esta agrupada en una clase,
	// pero se estaria violando el segundo principio
	// de SOLID -> OCP , ya que si esta abierto para la extension se tendria que
	// modificar la funcionalidad original
	// para poder solucionar este problema, se usaria herencia, polimorfismo,
	// abstraccion; en este caso particular se crearia una interfaz
	// con los metodos de calcular el area y perimetro y se implementara

//	public double calcular(Rectangle rec) {
//		double perim = 0d;
//		perim = rec.getHeight() * 2 + rec.getWidth() * 2;
//		return perim;
//	}

	// funcion corregida con la interfaz
	// con esta funcinoalidad se cumple el segundo principio de SOLID (OCP),es decir
	// no importa que clase ingrese al metodo , con tal que implemente la interfaz
	// se
	// cumplira de solo modificar las nuevas clases y no se modificaria este metodo
	// original
	public static double calcular(IFunPerimeter shape) {
		double area = 0d;
		area = shape.perimetro();
		return area;
	}
}
