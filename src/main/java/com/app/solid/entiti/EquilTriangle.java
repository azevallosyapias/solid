package com.app.solid.entiti;

import com.app.solid.service.IGeometricShape;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class EquilTriangle extends Shape {

	@Getter
	@Setter
	private double sides = 3;

	@Getter
	@Setter
	private double base;

	@Getter
	@Setter
	private double height;

	@Override
	public double area() {
		return base * height / 2;
	}

	@Override
	public double perimetro() {
		// TODO Auto-generated method stub
		return base * 3;
	}

}
