package com.app.solid.entiti;

import com.app.solid.service.IGeometricShape;

import lombok.Getter;
import lombok.Setter;

public class Square extends Shape {

	@Getter
	@Setter
	private double height;

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return height*height;
	}

	@Override
	public double perimetro() {
		// TODO Auto-generated method stub
		return height*4;
	}

}
