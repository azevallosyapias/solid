package com.app.solid.entiti;

import com.app.solid.service.IGeometricShape;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class Rectangle extends Shape {

	@Getter
	@Setter
	private double sides = 4;

	@Getter
	@Setter
	private double height;

	@Getter
	@Setter
	private double width;

	public Rectangle() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double area() {
		return height * width;
	}

	@Override
	public double perimetro() {
		return (height + width) * 2;
	}

	// violacion del principio de RESPONSABILIDAD UNICA
	// de acuerdo al principio S (Responsabilidad unica), esta clase solo deberia
	// existir para crear el rectangulo y estas
	// operaciones deberian estar en otra clase ya que representa la logica propia
	// de la aplicacion, para solucionar esto, se
	// desacopla en dos clases AreaOper y PerimOper para poder cumplir con este
	// principio de SOLID
//	public static double sumAreas(Rectangle rec) {
//		double area = 0d;
//		area = rec.getHeight() * rec.getWidth();
//		return area;
//	}

//	public static double sumPerim(Rectangle rec) {
//		double perim = 0d;
//		perim = rec.getHeight()*2+rec.getWidth()*2;
//		return perim;
//	}

}
